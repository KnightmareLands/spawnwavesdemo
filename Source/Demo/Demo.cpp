// Copyright 2018-Future norlin (myxaxaxa@gmail.com). All Rights Reserved.

#include "Demo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Demo, "Demo" );
